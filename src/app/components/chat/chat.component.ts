import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//para consumir el servidor
import { WebSocketService } from '../../services/web-socket.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit {
  userChat = {
    User: "",
    text: "",
  }

  myMessages:any;
  eventName = 'send-message';

  constructor(
   private activate: ActivatedRoute,
    private webService: WebSocketService) {};

  ngOnInit(): void {
    const id = this.activate.snapshot.params['id'];
   this.userChat.User = id;

this.webService.listen("text-event").subscribe((data)=>{
  this.myMessages=data;
 })
  }
  myMessage(){
    this.webService.emit(this.eventName, this.userChat);
    this.userChat.text = ''
  }
}
